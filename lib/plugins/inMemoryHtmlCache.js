var cache_manager = require('cache-manager');
var redis_store = require('cache-manager-redis');
module.exports = {
    init: function() {
        this.cache = cache_manager.caching({
            store: redis_store,
	    db: 0,
	    ttl: 21600
        });
    },

    beforePhantomRequest: function(req, res, next) {
        this.cache.get(req.prerender.url, function (err, result) {
            if (!err && result) {
                res.send(200, result);
            } else {
                next();
            }
        });
    },

    afterPhantomRequest: function(req, res, next) {
        this.cache.set(req.prerender.url, req.prerender.documentHTML);
        next();
    }
}
